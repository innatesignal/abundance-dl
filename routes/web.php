<?php

use App\Http\Controllers\DownloadedVideoController;
use App\Http\Controllers\ExecutionController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\QueueController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect()->route('jobs.all');
})->name('home');
Route::get('/jobs/failed', [ExecutionController::class, 'listFailed'])->name('jobs.failed');
Route::get('/jobs/successful', [ExecutionController::class, 'listSuccessful'])->name('jobs.successful');
Route::get('/jobs/all', [ExecutionController::class, 'listAll'])->name('jobs.all');
Route::post('/jobs/clear', [ExecutionController::class, 'clear'])->name('jobs.clear');
Route::post('/jobs/{id}/delete', [ExecutionController::class, 'delete'])->name('jobs.delete');

Route::get('/videos', [DownloadedVideoController::class, 'list'])->name('videos.list');

Route::get('/queue', [QueueController::class, 'list'])->name('queue.list');
