<?php

namespace App\Providers;

use App\Services\Abundance;
use Illuminate\Support\ServiceProvider;

class AbundanceServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Abundance::class, function ($app) {
            return new Abundance();
        });
    }
}
