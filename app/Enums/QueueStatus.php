<?php

namespace App\Enums;

enum QueueStatus: int
{
    case QUEUED = 0;
    case DOWNLOADING = 1;
    case COMPLETE = 2;
    case FAILED = 3;

    public function colorClass(): string
    {
        return match ($this) {
            self::QUEUED => 'warning',
            self::DOWNLOADING => 'primary',
            self::COMPLETE => 'success',
            self::FAILED => 'danger',
        };
    }

    public function string(): string
    {
        return match ($this) {
            self::QUEUED => 'Queued',
            self::DOWNLOADING => 'Downloading',
            self::COMPLETE => 'Complete',
            self::FAILED => 'Failed',
        };
    }
}
