<?php

namespace App\Enums;

enum ExecutionStatus: int
{
    case PROCESSING = 0;
    case SUCCESS = 1;
    case FAILED = 2;
    case CANCELLED = 3;

    public function colorClass(): string
    {
        return match ($this) {
            self::PROCESSING => 'warning',
            self::SUCCESS => 'success',
            self::FAILED => 'danger',
            self::CANCELLED => 'info'
        };
    }

    public function string(): string
    {
        return match ($this) {
            self::PROCESSING => 'Processing',
            self::SUCCESS => 'Success',
            self::FAILED => 'Failed',
            self::CANCELLED => 'Cancelled'
        };
    }
}
