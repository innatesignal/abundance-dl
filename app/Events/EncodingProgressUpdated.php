<?php

namespace App\Events;

use App\Models\ExecutionLog;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class EncodingProgressUpdated implements ShouldBroadcast
{
    use SerializesModels, Dispatchable, InteractsWithSockets;

    public ExecutionLog $execution;

    public $percentage;

    public function __construct(ExecutionLog $execution, $percentage)
    {
        $this->execution = $execution;
        $this->percentage = $percentage;
    }

    public function broadcastOn()
    {
        if ($this->execution) {
            return new Channel('execution.' . $this->execution->id);
        }
    }
}
