<?php

namespace App\Traits;

use Psr\Http\Message\ResponseInterface;

trait RequestTrait
{
    public function getRequest(string $url, array $query = [])
    {
        return $this->client->get($url, [
            'headers' => $this->headers,
            'query' => $query
        ]);
    }
    private function formatResponse(ResponseInterface $response)
    {
        return json_decode($response->getBody());
    }

    public function getEndpointData(string $url, array $query = [])
    {
        try {
            return $this->formatResponse(
                $this->getRequest(
                    $url,
                    $query
                )
            );
        } catch (\Exception $e) {
            return response()->json('Failed to get data from ' . $url . '. ' . $e->getMessage(), 500);
        }
    }
}
