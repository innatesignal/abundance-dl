<?php

namespace App\Services;

use App\Enums\ExecutionStatus;
use App\Enums\QueueStatus;
use App\Formats\HEVC;
use App\Models\DownloadedVideo;
use App\Models\ExecutionLog;
use GuzzleHttp\Client;
use App\Traits\RequestTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Models\QueuedVideo;
use App\Models\RemoteCheckLog;
use GuzzleHttp\Handler\StreamHandler;
use GuzzleHttp\HandlerStack;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;

class Abundance
{
    use RequestTrait;

    const LOGIN_URL = 'https://abundanceplus.com/api/sessions';
    const VIDEO_DETAIL_URL = 'https://abundanceplus.com/api/chapters';
    const CATEGORY_VIDEOS_URL = 'https://api-u-alpha.global.ssl.fastly.net/api/contents/search';
    const CATEGORY_LIST_URL = 'https://api-u-alpha.global.ssl.fastly.net/api/categories';

    protected $client;

    private $execution;

    private $success_count;

    protected $headers = [
        'X-Fastly-Origin' => 'abundanceplus'
    ];

    public function __construct()
    {
        // Registers shutdown handler
        $this->registerShutdown();

        // Creates new execution log record
        $this->startExecution();

        // Sets up filesystem folders as needed
        $this->setupFolders();

        // Initializes Guzzle client (cookies required due to lack of token functionality)
        $handler = new StreamHandler();
        $stack = HandlerStack::create($handler);
        $this->client = new Client([
            'cookies' => true,
            'handler' => $stack
        ]);
        $this->login();
    }

    public function registerShutdown()
    {
        pcntl_signal(SIGINT, function () {
            $this->shutdown(ExecutionStatus::CANCELLED);
            die;
        });
        register_shutdown_function([&$this, "shutdown"]);
    }

    public function shutdown(ExecutionStatus $status = ExecutionStatus::FAILED)
    {
        // Finalizes execution log record
        if ($this->execution) {
            $this->endExecution($status);
        }

        // Clears temp folder
        $this->cleanupTempFiles();
    }

    private function setupFolders()
    {
        if (!is_dir(storage_path("app/media"))) {
            Storage::makeDirectory("media");
        }

        if (!is_dir(storage_path("app/temp"))) {
            Storage::makeDirectory("temp");
        }
    }
    private function cleanupTempFiles()
    {
        if (is_dir(storage_path("app/temp"))) {
            File::cleanDirectory(storage_path("app/temp"));
        }
    }

    public function login()
    {
        $response = $this->client->request('POST', self::LOGIN_URL, [
            'timeout' => 30,
            'form_params' => [
                'email' => config('abundance.user.email'),
                'password' => config('abundance.user.password')
            ]
        ]);

        if (!match ($response->getStatusCode()) {
            200, 201 => true,
            default => false
        }) {
            throw new \Exception("Could not log in, check username or password. Otherwise, your subscription may have expired.");
        }
    }

    public function getCategories()
    {
        return $this->getEndpointData(self::CATEGORY_LIST_URL);
    }

    public function getCategoryVideos(int $category_id)
    {
        return $this->getEndpointData(self::CATEGORY_VIDEOS_URL, ['category_id' => $category_id]);
    }

    public function getVideoDetail(int $content_id)
    {
        return $this->getEndpointData(self::VIDEO_DETAIL_URL, ['content_id' => $content_id]);
    }

    public function compressVideo($video_file, $video_data, $show)
    {

        $newFile = "$show - $video_data->title.mkv";

        $completed_file = FFMpeg::open($video_file)
            ->export()
            ->inFormat(new HEVC)
            ->addFilter([
                '-preset', config('abundance.encoding_preset'),
                '-metadata', 'title="' . $video_data->title . '"',
                '-metadata', 'show="' . $show . '"'
            ])
            ->onProgress(function ($percentage) use ($video_data, $newFile) {
                echo 'Compressing ' . $video_data->title . ' - ' . $percentage . '%' . PHP_EOL;
                $this->execution->current_file = $newFile;
                $this->execution->current_file_percentage = $percentage;
                $this->execution->save();
            })
            ->save('temp/' . $newFile);

        if ($completed_file) {
            if (Storage::exists("media/$newFile")) {
                Storage::delete("media/$newFile");
            }
            Storage::move("temp/$newFile", "media/$newFile");

            Storage::delete("temp/$newFile");

            Storage::delete($video_file);
        }
    }

    public function downloadVideo($queuedVideo)
    {
        try {
            $video = $queuedVideo;

            if ($video->download_url) {
                $queuedVideo->delete();

                $tempFile =  $video->slug . ".mp4";

                // Whole number to prevent excessive writing of progress updates to database
                // Only saves when a new whole number is reached
                $whole = 0;
                // Download file
                $response = $this->client->request('GET', $video->download_url, [
                    'sink' =>  storage_path("app/temp") . '/' . $tempFile,
                    'progress' => function ($downloadTotal, $downloadedBytes) use ($video, $tempFile, &$whole) {
                        $percentage = $downloadedBytes > 0 ? ($downloadedBytes / $downloadTotal) * 100 : 0;
                        if (ceil($percentage) == $whole) {
                            $whole += 1;
                            echo $video->title . ' - ' . $percentage . '%' . PHP_EOL;
                            $this->execution->current_file = $tempFile;
                            $this->execution->current_file_percentage = round($percentage);
                            $this->execution->save();
                        }
                    }
                ]);

                if ($response->getStatusCode() != 200) {
                    throw new \Exception('Failed to download "' . $tempFile . '"');
                }

                $show = $video->show;
                // Move and rename downloaded file to appropriate location
                if (!is_dir(storage_path("app/media/$show"))) {
                    Storage::makeDirectory("media/$show");
                }

                $newFile = "$show - $video->title.mp4";

                if (Storage::exists("media/$show/$newFile")) {
                    Storage::delete("media/$show/$newFile");
                }

                Storage::move("temp/" . $tempFile, "media/$show/$newFile");

                $size = Storage::size("media/$show/$newFile");

                DownloadedVideo::updateOrCreate([
                    'video_id' => $video->video_id,
                ], [
                    'execution_id' => $this->execution->id,
                    'video_id' => $video->video_id,
                    'content_id' => $video->content_id,
                    'title' => $video->title,
                    'slug' => $video->slug,
                    'content_created_at' => $video->created_at,
                    'download_url' => $video->download_url,
                    'description' => $video->description,
                    'filesize' => $size
                ]);

                $this->success_count += 1;
            }
        } catch (\Exception $e) {

            return $e;
        }
    }


    public function fetchRemoteData()
    {
        QueuedVideo::truncate();

        foreach ($this->getCategories() as $category) {

            foreach ($this->getCategoryVideos($category->id) as $contents) {

                foreach ($this->getVideoDetail($contents->id) as $video) {

                    $already_downloaded = DownloadedVideo::where('video_id', $video->id)->first();

                    if (!$already_downloaded && $video->downloadable && $video->chapter_type == 'video' && $video->download_url) {
                        QueuedVideo::updateOrCreate(['video_id', $video->id], [
                            'video_id' => $video->id,
                            'status' => QueueStatus::QUEUED,
                            'content_id' => $video->content_id,
                            'title' => str_replace('/', '-', $video->title),
                            'content_created_at' => $video->created_at,
                            'description' => $video->description,
                            'show' => ucwords(strtolower($category->title)),
                            'slug' => $video->subject->permalink,
                            'download_url' => $video->download_url,
                            'current_download_percentage' => 0
                        ]);
                    }
                }
            }
        }

        RemoteCheckLog::create(['data_last_fetched' => Carbon::now()]);
    }

    public function downloadVideos()
    {
        try {
            $last_remote_check = RemoteCheckLog::orderByDesc('data_last_fetched')->first();
            if (!$last_remote_check || Carbon::now()->gt(
                Carbon::parse($last_remote_check->data_last_fetched)->addMinutes(config('abundance.remote_fetch_timeout'))
            )) {
                $this->fetchRemoteData();
            }

            QueuedVideo::all()->each(function ($video) {
                $this->downloadVideo($video);
            });

            $this->shutdown(ExecutionStatus::SUCCESS);
        } catch (\Exception $e) {
            $this->endExecution(ExecutionStatus::FAILED, $e->getMessage());
            dd($e->getMessage());
        }
    }

    private function startExecution()
    {
        $this->success_count = 0;

        $this->execution = ExecutionLog::create([
            'status' => ExecutionStatus::PROCESSING,
            'start_timestamp' => Carbon::now()->timestamp
        ]);
    }
    private function endExecution(ExecutionStatus $status, string $errorMessage = null)
    {
        if (!$this->execution) {
            return;
        }
        $this->execution->status = $status;
        $this->execution->completed_timestamp = Carbon::now()->timestamp;
        $this->execution->success_count = $this->success_count;
        $this->execution->message = $errorMessage;

        $this->execution->save();

        $this->execution = null; // Detach execution record from local property
    }
}
