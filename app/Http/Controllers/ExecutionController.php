<?php

namespace App\Http\Controllers;

use App\Models\ExecutionLog;
use App\Enums\ExecutionStatus;

class ExecutionController extends Controller
{
    public function list(ExecutionStatus $status = null)
    {
        return view('executions.running', ['executions' => ExecutionLog::where('status', $status)->orderByDesc('start_timestamp')->take(15)->get()]);
    }
    public function listRunning()
    {
        return $this->list(ExecutionStatus::PROCESSING);
    }
    public function listFailed()
    {
        return $this->list(ExecutionStatus::FAILED);
    }
    public function listSuccessful()
    {
        return $this->list(ExecutionStatus::SUCCESS);
    }
    public function listAll()
    {
        return view('executions.running', ['executions' => ExecutionLog::orderByDesc('start_timestamp')->take(15)->get()]);
    }

    public function delete(ExecutionLog $id)
    {
        $id->delete();
        return back();
    }

    public function clear()
    {
        ExecutionLog::truncate();
        return back();
    }

    public function running()
    {
        return ExecutionLog::processing()->get();
    }
}
