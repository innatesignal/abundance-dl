<?php

namespace App\Http\Controllers;

use App\Models\DownloadedVideo;

class DownloadedVideoController extends Controller
{
    public function list()
    {
        return view('videos/list', ['videos' => DownloadedVideo::all()]);
    }
}
