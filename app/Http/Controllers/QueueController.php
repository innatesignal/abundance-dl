<?php

namespace App\Http\Controllers;

use App\Models\QueuedVideo;

class QueueController extends Controller
{
    public function list()
    {
        return view('queue/list', ['queued' => QueuedVideo::all()]);
    }
}
