<?php

namespace App\Http\Controllers;

use App\Models\DownloadedVideo;
use Illuminate\Support\Facades\Route;

class HomeController extends Controller
{
    public function list()
    {
        $routes = Route::getRoutes();
        foreach ($routes as $route) {
            dd($route->getName());
        };
        return view('videos/list', ['videos' => DownloadedVideo::all()]);
    }
}
