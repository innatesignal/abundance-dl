<?php

namespace App\Console\Commands;

use App\Services\Abundance;
use Illuminate\Console\Command;

class DownloadVideos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'abundance:download_videos {content_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download video(s) from Abundance site';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param  \App\Services\Abundance $services
     * @return mixed
     */
    public function handle(Abundance $service)
    {
        try {
            $service->downloadVideos();
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
}
