<?php

namespace App\Models;

use App\Enums\ExecutionStatus;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ExecutionLog extends Model
{
    protected $table = 'execution_log';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    public $timestamps = false;

    protected $fillable = [
        'status',
        'success_count',
        'start_timestamp',
        'current_file',
        'current_file_progress',
        'completed_timestamp',
    ];

    protected $casts = [
        'status' => ExecutionStatus::class
    ];

    protected function scopeProcessing($query)
    {
        return $query->where('status', ExecutionStatus::PROCESSING);
    }

    public function getStartedAttribute()
    {
        return Carbon::parse($this->start_timestamp)->toDateTimeString();
    }
}
