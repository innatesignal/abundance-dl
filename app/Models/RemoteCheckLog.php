<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RemoteCheckLog extends Model
{
    protected $table = 'remote_check_log';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    public $timestamps = false;

    protected $fillable = [
        'data_last_fetched',
    ];
}
