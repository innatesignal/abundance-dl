<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QueuedVideo extends Model
{
    protected $table = 'download_queue';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'status',
        'show',
        'episode',
        'video_id',
        'content_id',
        'title',
        'slug',
        'download_url',
        'current_download_percentage',
        'description',
        'content_created_at'
    ];
}
