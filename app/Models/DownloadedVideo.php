<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DownloadedVideo extends Model
{
    protected $table = 'downloaded_videos';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'execution_id',
        'video_id',
        'content_id',
        'title',
        'slug',
        'content_created_at',
        'download_url',
        'description',
        'filesize'
    ];
}
