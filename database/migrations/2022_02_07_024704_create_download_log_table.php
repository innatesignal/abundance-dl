<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDownloadLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('execution_log', function (Blueprint $table) {
            $table->id();
            $table->integer('status');
            $table->integer('success_count')->default(0);
            $table->string('message')->nullable();
            $table->string('current_file')->nullable();
            $table->float('current_file_percentage')->default(0.0);
            $table->integer('start_timestamp');
            $table->integer('completed_timestamp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('execution_log');
    }
}
