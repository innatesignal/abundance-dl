<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDownloadedVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('downloaded_videos', function (Blueprint $table) {
            $table->id();
            $table->integer('execution_id')->nullable();
            $table->integer('video_id')->unique();
            $table->integer('content_id');
            $table->string('title');
            $table->string('slug', 100);
            $table->integer('content_created_at')->nullable();
            $table->string('download_url');
            $table->integer('filesize')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
            $table->foreign('execution_id', 'downloaded_videos_execution_id')->references('id')->on('execution_log')->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('downloaded_videos');
    }
}
