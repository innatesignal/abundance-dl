<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDownloadQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('download_queue', function (Blueprint $table) {
            $table->id();
            $table->integer('status');
            $table->string('show');
            $table->integer('episode')->default(1);
            $table->integer('video_id');
            $table->integer('content_id');
            $table->string('title');
            $table->string('slug', 100);
            $table->integer('content_created_at')->nullable();
            $table->text('description')->nullable();
            $table->string('download_url');
            $table->float('current_download_percentage')->default(0.0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('download_queue');
    }
}
