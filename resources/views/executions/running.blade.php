@extends('template')

@section('page-title')
Abundance+ Downloads
@endsection

@section('head-append')
<script type="text/javascript">
function deleteLog(id){
    axios.post(`/jobs/${id}/delete`).then((resp)=>{
        window.location.reload();
    });
}
function clearLogs(){
    axios.post(`/jobs/clear`).then((resp)=>{
        window.location.reload();
    });
}
</script>
@endsection

@section('content')
<div class="container-fluid p-4">
    @if($executions->count() == 0)
    <h1 class="text-center">No Jobs Running</h1>
    @else
    <h1 class="text-center">Running Jobs
        <button type="button" class="btn" onclick="clearLogs()">Clear</button>
    </h1>
    <table class="table">
        <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Current File</th>
              <th scope="col">Progress</th>
              <th scope="col">Execution Status</th>
              <th scope="col">Message</th>
              <th scope="col">Timestamp</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
        @foreach($executions as $execution)
        <tr>
            <th scope="row">{{$execution->id}}</th>
            <td>{{$execution->current_file}}</td>
            <td width="15%">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: {{$execution->current_file_percentage}}%" aria-valuenow="{{$execution->current_file_percentage}}" aria-valuemin="0" aria-valuemax="100">{{$execution->current_file_percentage ?? 0}}%</div>
                  </div>
            </td>
            <td id="execution-{{$execution->id}}-pct"><span class="badge bg-{{$execution->status->colorClass()}}">{{$execution->status->string()}}</span></td>
            <td>{{$execution->message}}</td>
            <td>{{$execution->started}}</td>
            <td width="2rem">
                <button type="button" class="btn-close" aria-label="Close" onclick="deleteLog({{$execution->id}})"></button>
            </td>
        </tr>
        @endforeach
          </tbody>
    </table>
    @endif
</div>

@endsection
