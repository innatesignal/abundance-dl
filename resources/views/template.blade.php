<!doctype html>
<html lang="en">
  <head>
    @include('head')
    @yield('head-append')
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a href="{{route('jobs.all')}}" class="nav-link">Job Log</a>
            </li>
            <li class="nav-item">
                <a href="{{route('videos.list')}}" class="nav-link">Downloaded Videos</a>
            </li>
            <li class="nav-item">
                <a href="{{route('queue.list')}}" class="nav-link">Queued Videos</a>
            </li>
        </ul>
    </div>
    </nav>
    @yield('content')
  </body>
</html>
