@extends('template')

@section('page-title')
Abundance+ Downloaded Videos
@endsection

@section('head-extend')

@endsection

@section('content')
<div class="container p-4">
    @if($queued->count() == 0)
    <h1 class="text-center">No Videos Queued</h1>
    @else
    <h1 class="text-center">Queued Videos</h1>
    <table class="table">
        <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Show</th>
              <th scope="col">Title</th>
              <th scope="col">Description</th>
            </tr>
          </thead>
          <tbody>
        @foreach($queued as $video)
        <tr>
            <th scope="row">{{$video->id}}</th>
            <td>{{$video->title}}</td>
            <td>{{$video->slug}}</td>
            <td>
                {{strlen($video->description) > 200 ? substr($video->description,0,200)."..." : $video->description}}
            </td>
        </tr>
        @endforeach
          </tbody>
    </table>
    @endif
</div>
@endsection
