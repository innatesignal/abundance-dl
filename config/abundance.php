<?php
return [
    'user' => [
        'email' => env('USER_EMAIL'),
        'password' => env('USER_PASSWORD'),
    ],

    'remote_fetch_timeout' => env('MEDIA_REMOTE_FETCH_TIMEOUT', 30),
    'poll_frequency' => env('MEDIA_POLL_FREQUENCY'),
    'compress_media' => env('MEDIA_COMPRESS'),
    'encoding_preset' => env('MEDIA_ENCODING_PRESET', 'ultrafast'),
];
