# Abundance-DL

Downloads videos from Abundance+ for registered users

Configuration:
`USER_EMAIL` - User email for login
`USER_PASSWORD` - User password for login

`MEDIA_POLL_FREQUENCY` - How often (minutes) to check for new updates
`MEDIA_REMOTE_FETCH_TIMEOUT` - How often should new remote data be pulled? (minutes)
`MEDIA_COMPRESS` - Should media be compressed? (uses FFMPEG)
`FFMPEG_PRESET` - Preset to use for FFMPEG compression

Mounts:
`/var/www/html/database/db` - Sqlite database path
`/var/www/html/storage/app/temp` - Temporary download location
`/var/www/html/storage/app/media` - Permanent download location
